# Projet Portfolio Juliette

 Mon wireframe et de ma maquette sont consultables sur Figma : https://www.figma.com/file/uW8tXXjiORHpMtohtvbMEF/Portfolio-Juliette?node-id=0%3A1&t=vBUMUStPAGGdUNYK-1

Le projet comporte une barre de navigation verticale : des ancres permettent de naviguer au sein des différentes sections de la landing page.
Sous la nav bar, un hero, une section d'accroche, permet de mettre le titre h1 et la site et d'utiliser un background libre de droit avec un effet de paralaxe.

La seconde section, A propos, contient une petite présentation et une photo. Texte et photo sont côte à côte sur desktop et l'un au-dessus de l'autre sur mobile.

La troisième section, sur les compétences, a été réalisé à partir de composants "list-group" de bootstrap, avec quelques ajustements. Les compétences technologiques sont réparties en cartes par thématiques. A l'intérieur de ces cartes, on trouve pour chaque technologie le titre, le niveau de maîtrise, qui pourra progresser tout au long de l'année, et une icônes d'awesome font.

La quatrième section est une liste de projets, adaptée à partir de composants cartes de bootstrap. Il n'y a qu'un seul projet réalisé pour le moment mais d'autres pourront venir au fur et à mesure. Toutes ces cartes et celles de la section précédente utilisent le système de colonnes bootstrap, ce qui permet de les placer les unes au dessous des autres en mobile.

La cinquième section permet de proposer des moyens de contact. Elle comporte tout simplement un mail, un numéro de téléphone et un lien vers Gitlab. 

Enfin, un petit footer conclut cette page.

Le site a été mis en ligne en utilisant Netlify, et un lien a été créé depuis le site de la promo de Simplon pour y mener.



* Consigne ci-dessous


# Projet Portfolio

Faire votre portfolio pour le site simplonlyon.fr dans lequel devront figurés :
* Une petite présentation
* La liste de vos projets
* Un contact
* Le lien vers votre gitlab
* Eventuellement vos préférences en terme de langages/techno

Le site devra être responsive et valide W3C. Il est fortement conseillé d'utiliser la grid et les components bootstraps.

Valider également l'accessibilité du site avec une extension comme WAVE.

## Conception
Le projet devra commencer par avoir : 
* La ou les maquettes fonctionnelles du site (qui doivent en gros indiquée les éléments d'interface et leur positionnements)
* Un projet gitlab avec un README qui décrit le projet puis qui contiendra à terme vos maquettes
* Faire la structure HTML complète de l'application avant de commencer le style

## Mise en ligne

1. Faire en sorte de mettre le site en ligne en utilisant les Gitlab Pages.
2. Mettre le lien de votre Gitlab Pages dans votre profil sur simplonlyon.fr